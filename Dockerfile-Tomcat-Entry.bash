#!/bin/bash

set -e

java -Djava.security.egd=file:/dev/./urandom -jar /usr/local/tomcat/webapps/java_backend_example_app-1.0-SNAPSHOT.jar

exec "/usr/local/tomcat/bin/catalina.sh" "$@"

